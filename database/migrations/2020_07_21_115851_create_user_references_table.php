<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_references', function (Blueprint $table) {
            $table->id('id');
            $table->integer('userID');
            $table->string('jobDescription')->nullable()->default('none');
            $table->string('referencePersonnel')->nullable()->default('none');
            $table->string('referencePersonnelEmail')->nullable()->default('none');
            $table->timestamps();

            $table->foreign('userID')->references('userID')->on('users')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_references');
    }
}
