<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id('id');
            $table->string('title');
            $table->integer('authorID');
            $table->longText('column');
            $table->string('coverImage')->nullable()->default('articles_image_wireframe');
            $table->string('imageCourtsy')->nullable()->default('unknown');
            $table->string('description');
            $table->timestamps();

            $table->foreign('authorID')->references('userID')->on('users')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
