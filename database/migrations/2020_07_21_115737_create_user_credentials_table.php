<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_credentials', function (Blueprint $table) {
            $table->id('id');
            $table->integer('userID');
            $table->string('educationalBackground')->default('none');
            $table->string('occupation')->default('none');
            $table->string('fieldOfExpertise')->default('none');
            $table->string('fieldOfInterest')->default('none');
            $table->timestamps();

            $table->foreign('userID')->references('userID')->on('users')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_credentials');
    }
}
