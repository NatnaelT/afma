<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('id');
            $table->string('fName');
            $table->string('lName');
            $table->date('bDate');
            // $table->char('sex');
            $table->enum('sex', ['male', 'female']);
            $table->string('userProfilePic')->nullable()->default('UPC_noimage.jpg');
            $table->string('frontHeadshot')->nullable()->default('FHS_noimage.jpg');
            $table->string('leftHeadshot')->nullable()->default('LHS_noimage.jpg');
            $table->string('rightHeadshot')->nullable()->default('RHS_noimage.jpg');
            // $table->string('previllage')->nullable()->default('client');
            $table->enum('previllage', ['Admin', 'Blogger', 'Client'])->nullable()->default('Client');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('pending_approval', ['pending', 'accepted', 'rejected']);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
