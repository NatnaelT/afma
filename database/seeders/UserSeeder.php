<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'fName' => 'AEFMA',
            'lName' => 'Admin',
            'bDate' => '00/00/0000',
            'sex' => 'Female',
            'previllage' => 'Admin',
            'email' => 'admin@aefma.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'fName' => 'AEFMA',
            'lName' => 'User',
            'bDate' => '00/00/0000',
            'sex' => 'Female',
            'previllage' => 'Client',
            'email' => 'user@aefma.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'remember_token' => Str::random(10)
        ]);


    }
}
