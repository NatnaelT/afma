<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
use App\UserCredential;
use App\UserReference;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call([
        //     UserSeeder::class,
        //     ]);

        User::create([
            'fName' => 'AEFMA',
            'lName' => 'Admin',
            'bDate' => '00/00/0000',
            'sex' => 'Female',
            'previllage' => 'Admin',
            'email' => 'admin@aefma.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'fName' => 'AEFMA',
            'lName' => 'User',
            'bDate' => '00/00/0000',
            'sex' => 'Female',
            'previllage' => 'Client',
            'email' => 'user@aefma.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'remember_token' => Str::random(10),
        ]);
        // User::factory()->count(200)->create();
        for($i = 1; $i <= 200; $i++){
            User::factory()->create();
            UserCredential::factory()->create(['userID' => $i]);
            UserReference::factory()->create(['userID' => $i]);
        }
    }
}
