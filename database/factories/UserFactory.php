<?php

// /** @var \Illuminate\Database\   Eloquent\Factory $factory */

namespace Database\Factories;

use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $factory->define(User::class, function (Faker $faker) {
//     $gender = $faker->randomElement(['Male', 'Female']);
//     return [
//         'fname' => $faker->firstName($gender),
//         'lname' => $faker->lastName(),
//         'bDate' => $faker->date('d-m-Y'),
//         'sex' => $faker->$gender,
//         'previllage' => $faker->randomElement(['Admin', 'Blogger', 'Client']),
//         'email' => $faker->unique()->safeEmail,
//         'email_verified_at' => now(),
//         'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//         'remember_token' => Str::random(10),
//     ];
// });

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'fname' => $this->faker->firstName(),
            'lname' => $this->faker->lastName(),
            'bDate' => $this->faker->date('d-m-Y'),
            'sex' => $this->faker->randomElement(['male', 'female']),

            // 'userProfilePic' => 'https://image.freepik.com/free-vector/3d-realistic-faceless-human-model_1441-2189.jpg',
            // 'userProfilePic' =>$this->faker->image('public/storage/images/avatars', 360, 360, 'humans', true, true, 'headshots', true),
            'userProfilePic' => 'noimage.jpg',
            'frontHeadshot' => 'noimage.jpg',
            'leftHeadshot' => 'noimage.jpg',
            'rightHeadshot' => 'noimage.jpg',
            'previllage' => $this->faker->randomElement(['Admin', 'Blogger', 'Client']),
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'pending_approval' => $this->faker->randomElement(['pending', 'approved', 'rejected']),
            'remember_token' => Str::random(10),
        ];
    }
}
