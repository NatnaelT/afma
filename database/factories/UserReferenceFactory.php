<?php

namespace Database\Factories;

use App\UserReference;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserReferenceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserReference::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            // 'userID' => $this->faker->numberBetween(1, 201),
            'jobDescription' => $this->faker->paragraph(),
            'referencePersonnel' => $this->faker->name($this->faker->randomElement(['male', 'female'])),
            'referencePersonnelEmail' => $this->faker->email
        ];
    }
}
