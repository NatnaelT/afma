<?php

namespace Database\Factories;

use App\UserCredential;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserCredentialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserCredential::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'userID' => $this->faker->numberBetween(1, 201),
            'educationalBackground' =>$this->faker->randomElement(['None',
                                                                    'Elemetary School Graduate',
                                                                    'Highschool Graduate',
                                                                    'Preparatory School Graduate',
                                                                    'University Graduate',
                                                                    'TVET Graduate'
                                                                ]),
            'occupation' => $this->faker->randomElement(['Student',
                                                            'Fulltime Employee',
                                                            'Business Owner'
                                                        ]),
            'fieldOfExpertise' => $this->faker->randomElement(['Script Writing',
                                                                    'Film Producing',
                                                                    'Production Management',
                                                                    'Directing',
                                                                    'Camera Operation',
                                                                    'Sound Engineering',
                                                                    'Mastering',
                                                                    'Sound Effects Designing',
                                                                    'Visual Effects Designing',
                                                                    'Editing',
                                                                    'Graphics Designing',
                                                                    'Financial Management'
                                                                ]),
            'fieldOfInterest' => $this->faker->randomElement(['Script Writing',
                                                                    'Film Producing',
                                                                    'Production Management',
                                                                    'Directing',
                                                                    'Camera Operation',
                                                                    'Sound Engineering',
                                                                    'Mastering',
                                                                    'Sound Effects Designing',
                                                                    'Visual Effects Designing',
                                                                    'Editing',
                                                                    'Graphics Designing',
                                                                    'Financial Management'
                                                                ]),
        ];
    }
}
