@extends('layouts.app')

@section('content')
<div class="container py-md-5">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    <div class="mb-md-0">{{ __('Register') }}</div>
                    <div class="mt-md-1" style="color: gray !important"><small>(Required fields have been marked with <i style="color: red !important"><b>*</b></i> )</small></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="fName" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="fName" type="text" class="form-control @error('fName') is-invalid @enderror" name="fName" value="{{ old('fName') }}" required autocomplete="given-name" autofocus>

                                        @error('fName')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lName" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="lName" type="text" class="form-control @error('lName') is-invalid @enderror" name="lName" value="{{ old('lName') }}" required autocomplete="family-name" autofocus>

                                        @error('lName')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="datepicker" class="col-md-4 col-form-label text-md-right">{{ __('Birth Date') }}<sup style="color: red !important">*</sup></label>

                                    <div class="input-group col-md-6">
                                        <input id="datepicker" type="date" class="date datepicker form-control @error('bDate') is-invalid @enderror" data-provide="datepicker" name="bDate" value="{{ old('bDate') }}" required autocomplete="bDate" autofocus>

                                        @error('bDate')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="sex" class="col-md-4 col-form-label text-md-right">{{ __('Sex') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <select class="form-control @error('sex') is-invalid @enderror" name="sex" value="{{ old('sex') }}" required autofocus>
                                            <option value="" class="text-muted">Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>

                                        @error('sex')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- <div class="form-group row">
                                    <label for="userProfilePic" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="userProfilePic" type="file" class="form-control @error('userProfilePic') is-invalid @enderror" name="userProfilePic" required>

                                        @error('userProfilePic')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="frontHeadshot" class="col-md-4 col-form-label text-md-right">{{ __('Front Headshot') }}</label>

                                    <div class="col-md-6">
                                        <input id="frontHeadshot" type="file" class="form-control @error('frontHeadshot') is-invalid @enderror" name="frontHeadshot">

                                        @error('frontHeadshot')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="leftHeadshot" class="col-md-4 col-form-label text-md-right">{{ __('Left Headshot') }}</label>

                                    <div class="col-md-6">
                                        <input id="leftHeadshot" type="file" class="form-control @error('leftHeadshot') is-invalid @enderror" name="leftHeadshot">

                                        @error('leftHeadshot')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="rightHeadshot" class="col-md-4 col-form-label text-md-right">{{ __('Right Headshot') }}</label>

                                    <div class="col-md-6">
                                        <input id="rightHeadshot" type="file" class="form-control @error('rightHeadshot') is-invalid @enderror" name="rightHeadshot">

                                        @error('rightHeadshot')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div> --}}

                                <div class="form-group row">
                                    <label for="educationalBackground" class="col-md-4 col-form-label text-md-right">{{ __('Educational background') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="educationalBackground" type="text" class="form-control @error('educationalBackground') is-invalid @enderror" name="educationalBackground" value="{{ old('educationalBackground') }}" required autofocus>

                                        @error('educationalBackground')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="occupation" class="col-md-4 col-form-label text-md-right">{{ __('Occupation') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="occupation" type="text" class="form-control @error('occupation') is-invalid @enderror" name="occupation" value="{{ old('occupation') }}" required autofocus>

                                        @error('occupation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fieldOfExpertise" class="col-md-4 col-form-label text-md-right">{{ __('Field of Expertise') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="fieldOfExpertise" type="text" class="form-control @error('fieldOfExpertise') is-invalid @enderror" name="fieldOfExpertise" value="{{ old('fieldOfExpertise') }}" required autofocus>

                                        @error('fieldOfExpertise')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fieldOfInterest" class="col-md-4 col-form-label text-md-right">{{ __('Field of Interest') }}<sup style="color: red !important">*</sup></label>

                                    <div class="col-md-6">
                                        <input id="fieldOfInterest" type="text" class="form-control @error('fieldOfInterest') is-invalid @enderror" name="fieldOfInterest" value="{{ old('fieldOfInterest') }}" required autofocus>

                                        @error('fieldOfInterest')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0 mt-md-5">
                            <div class="float-md-right mr-md-5 ml-md-auto">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
