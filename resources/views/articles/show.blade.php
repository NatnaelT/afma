@extends('layouts.app')

@section('content')
    <!--Article Cover Image-->
    <div class="jumbotron show-jumbotron my-md-0 py-md-0">
        <div class="container mx-md-0 px-md-1">
            <img style="width: 100% !important" src="/storage/coverImages/{{$article->coverImage}}">
        </div>
    </div>

    <div class="row justify-content-md-between mt-md-0 pt-md-0 text-secondary">
        <div class="col-auto mr-auto">
            Posted by: <b class="font-weight-bolder text-dark">{{$article->user->fName}} {{$article->user->lName}}</b>
        </div>
        <div class="col-auto">
            <small>Date: {{$article->created_at}}</small>
        </div>
    </div>

    <div class="mt-md-4 mb-md-3 ml-md-5">
        <!--Article Title-->
        <h1>{{$article->title}}</h1>
        <div class="mt-md-4">
            <!--Article Body-->
            {!!$article->column!!}
        </div>
    </div>

    <hr>

    @if(!Auth::guest())
        @if(Auth::user()->id == $article->authorID)
            <div class="container">
                <a href="/articles/{{$article->id}}/edit" class="btn btn-outline-secondary">Edit</a>

                {!! Form::open(['action' => ['ArticlesController@destroy', $article->id], 'method' => 'POST', 'class' => 'inline pull-right float-right mr-md-5']) !!}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger float-right']) }}
                {!! Form::close() !!}
            </div>
        @endif
    @endif
@endsection
