@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1>Create Article</h1>
        <div class="container">
            {!! Form::open(['action' => 'ArticlesController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!--Article Title Input Field-->
                <div class="form-group">
                    {{Form::label('title', 'Title')}}
                    {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Article Title'])}}
                </div>

                <!--Article Description Input Field-->
                <div class="form-group">
                    {{Form::label('description', 'Description')}}
                    {{Form::text('description', '', ['class' => 'form-control', 'placeholder' => 'Article`s introductory statment goes here. This should not be more than 40 words.'])}}
                </div>

                <!--Article Body Input Field-->
                <div class="form-group">
                    {{Form::label('body', 'Column')}}
                    {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'The full article body goes here.'])}}
                </div>

                <!--Article Cover Image Input Field-->
                <div class="form-group">
                    <div class="custom-file">
                        {{Form::label('coverImage', 'Select cover image for the article', ['class' => 'custom-file-label'])}}
                        {{Form::file('coverImage', ['class' => 'form-control, custom-file-input'])}}
                    </div>
                </div>

                <!--Article Image Credit Input Field-->
                <div class="form-group">
                    {{Form::label('courtsy', 'Image Courtsy')}}
                    {{Form::text('courtsy', '', ['class' => 'form-control', 'placeholder' => 'Article Photo credit goes to ...'])}}
                </div>

                {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
