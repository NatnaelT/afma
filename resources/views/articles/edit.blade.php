@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1>Edit Article</h1>
        <div class="container">
            {!! Form::open(['action' => ['ArticlesController@update', $article->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!--Article Title Input Field-->
                <div class="form-group">
                    {{Form::label('title', 'Title')}}
                    {{Form::text('title', $article->title, ['class' => 'form-control', 'placeholder' => 'Article Title'])}}
                </div>

                <!--Article Description Input Field-->
                <div class="form-group">
                    {{Form::label('description', 'Description')}}
                    {{Form::text('description', $article->description, ['class' => 'form-control', 'placeholder' => 'Article`s introductory statment goes here. This should not be more than 40 words.'])}}
                </div>

                <!--Article Body Input Field-->
                <div class="form-group">
                    {{Form::label('body', 'Column')}}
                    {{Form::textarea('body', $article->column, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'The full article body goes here.'])}}
                </div>

                <!--Article Cover Image Input Field-->
                <div class="form-group">
                    <div class="custom-file">
                        {{Form::label('coverImage', 'Select cover image for the article', ['class' => 'custom-file-label'])}}
                        {{Form::file('coverImage', ['class' => 'form-control, custom-file-input'])}}
                    </div>
                </div>

                <!--Article Image Credit Input Field-->
                <div class="form-group">
                    {{Form::label('courtsy', 'Image Courtsy')}}
                    {{Form::text('courtsy', $article->imageCourtsy, ['class' => 'form-control', 'placeholder' => 'Article Photo credit goes to ...'])}}
                </div>

                <!--Spoof a PUT request from a POST method.-->
                {{Form::hidden('_method', 'PUT')}}

                {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
