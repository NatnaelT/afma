@extends('layouts.app')

@section('content')
    <!--Article Page Cover-->
    {{-- <div class="article-jumbotron">
        <div class="mx-5 py-3">
            <h1 class="mb-md-4">Articles</h1>
            <div class="ml-md-3">
                <h5>Filter by:</h5>
                <div class="row align-items-lg-end">
                    <div class="col-md-3 justify-content-md-start">
                        <p class="lead font-weight-bolder">Category</p>
                        <small>Selection drop-down form goes here.</small>
                    </div>
                    <div class="col-md-3 justify-content-md-start">
                        <p class="lead font-weight-bolder">Date</p>
                        <small>Selection drop-down form goes here.</small>
                    </div>
                    <div class="col-md-6 col-md-offset-3 justify-content-md-end">
                        <div class="row justify-content-md-end mr-4">
                            <div class="col-sm-8">
                                <form action="" class="search-form">
                                    <div class="form-group has-feedback">
                                        <label for="search" class="sr-only">Search</label>
                                        <input type="text" class="form-control" name="search" id="search" placeholder="search">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <!--Articles are in chronological order, with the latest going up first.-->
    <div class="mx-md-5 px-md-5 my-md-5 py-md-4">
        @if(count($articles) > 0)
            @foreach ($articles as $article)
                <div class="well">
                    <div class="mb-md-5 pb-md-2">
                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                            <div class="col-8 d-none d-lg-block ml-md-4 pr-md-2 my-md-3">
                            <h3 class="mb-0 font-weight-bold">{{$article->title}}</h3>
                            <div class="mb-1 text-muted mt-md-1"><small>Posted on: {{$article->created_at}}</small></div>
                            <div class="mb-1 text-muted mt-md-n2 mb-md-2 ml-md-2"><small class="font-weight-bolder">{{$article->user->fName}} {{$article->user->lName}}</small></div>
                            <p class="card-text mb-auto">{{$article->description}}</p>
                            <div class="mt-2"><a href="/articles/{{$article->id}}" class="stretched-link">Continue reading</a></div>
                            </div>

                            <div class="col d-flex flex-column position-static">
                                <img src="/storage/coverImages/{{$article->coverImage}}" width="200">
                                {{-- <svg class="bd-placeholder-img" width="361" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg> --}}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <!--Pagination-->
            <div class="row text-center">
                {{$articles->links()}}
            </div>

            @else
                <div class="badge-warning">
                    <p>No articles found.</p>
                </div>
        @endif
    </div>
@endsection
