@extends('layouts.app')

@section('content')

    <!--Headline cover on the landing page-->
    <div class="jumbotron heading">
        <div class="container mx-md-0 px-md-1">
            <h1 class="display-1 py-md-0 my-md-0 font-weight-bolder" style="color: #ffffff !important;">{{ __("Alatinos") }}</h1>
            <h3 style="color: #ffffff !important;">{{ __("Ethiopian Filmmakers' Association") }}</h3>
            <p class="lead my-5 mx-5 px-md-5 font-italic" style="color: #ffffff !important;">{{ __("Lets gather up in unity to raise Ethiopian Film with intellect and excellence..." )}}</p>
        </div>
    </div>

    <!--Keynote bullet points of AFMA-->
    <div class="container my-5 pt-5">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm text-center"  style="background-color: #94703b !important;">
                    <img src="/storage/assets/theatrefaces.png" width="200" class="center align-self-center justify-content-center my-3 py-2 mt-4">
                    <div class="card-body">
                        <p class="card-text lead font-weight-bold">{{ __("Looking for new talents for your next project?") }}</p>
                        <a class="btn btn-sm btn-outline-secondary" href="/members">{{ __("Browse Here") }}</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card mb-4 shadow-sm text-center" style="background-color: #94703b !important;">
                    <img src="/storage/assets/filmroll.png" width="200" class="center align-self-center justify-content-center my-3 py-2 mt-4">
                    <div class="card-body">
                        <p class="card-text lead font-weight-bold">{{ __("Want to stay-up-to-date with Ethiopian cinema?") }}</p>
                        <a class="btn btn-sm btn-outline-secondary" href="/articles">{{ __("Click Here") }}</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card mb-4 shadow-sm text-center" style="background-color: #94703b !important;">
                    <div class="text-center">
                        <img src="/storage/assets/logowireframe.png" height="117" class="center align-self-center justify-content-center my-3 py-2 mt-4">
                    </div>
                    <div class="card-body">
                        <p class="card-text lead font-weight-bold">{{ __("Looking to see what is new in Alatinos?") }}</p>
                        <a class="btn btn-sm btn-outline-secondary" href="/events" role="button">{{ __("Browse Here") }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Latest Event-->
    @if (count($events) > 0)
        @foreach ($events as $event)
        <div class="mx-md-5 px-md-5 mb-md-5 pb-md-3 event-card">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div class="col d-flex flex-column position-static">
                    <img src="/storage/eventCoverImages/{{$event->coverImage}}" height="250">
                </div>
                <div class="col-3 d-none d-lg-block mx-md-4 my-md-3">
                    <h3 class="mb-0 font-weight-bold">{{$event->title}}</h3>
                    <div class="mb-1 text-muted ml-md-3 mt-md-1 mb-md-4">{{ __("Starts on:") }} {{$event->startDate}}, {{ date('h:ia', strtotime($event->startTime)) }}</div>
                    <p class="card-text mb-auto">{{$event->description}}</p>
                    {{-- <div class="mt-2 ml-2"><a href="#" class="stretched-link">Continue reading</a></div> --}}
                </div>
            </div>
        </div>
        @endforeach
    @endif

    <!--Latest Article-->
    @if(count($articles) > 0)
        @foreach($articles as $article)
            <div class="mx-md-5 px-md-5 mb-md-5 pb-md-3">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col-3 d-none d-lg-block mx-md-4 my-md-3">
                        <h3 class="mb-0 font-weight-bold">{{$article->title}}</h3>
                        <div class="mb-1 text-muted mt-md-1"><small>{{ __("Posted on:") }} {{$article->created_at}}</small></div>
                        <div class="mb-1 text-muted mt-md-n2 mb-md-2"><small class="font-weight-bolder">{{$article->user->fName}} {{$article->user->lName}}</small></div>
                        <p class="card-text mb-auto">{{$article->description}}</p>
                        <div class="mt-2"><a href="/articles/{{$article->id}}" class="stretched-link">{{ __("Continue reading") }}</a></div>
                    </div>
                    <div class="col d-flex flex-column position-static">
                        <img src="/storage/coverImages/{{$article->coverImage}}" width="auto" height="200">
                    </div>
                </div>
            </div>
        @endforeach
    @endif

@endsection
