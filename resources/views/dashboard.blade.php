@extends('layouts.app')

@section('content')
<div class="container my-md-4">
    <!--Session check-->
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    @if (Auth::user()->userProfilePic == 'UPC_noimage.jpg')
        <div class="card border-warning my-5" style="background: rgba(245, 219, 104, 0.555)">
            <div class="card-body">
                <p style="color: rgb(209, 98, 7)">Add a profile picture for a better experience ...</p>
                <small style="color: rgb(209, 98, 7)">Go to '<a href="/users/{{Auth::user()->id}}/edit" class="stretched-link">Edit Profile</a>' page to add your picture.</small>
            </div>
        </div>
    @else
        @if (Auth::user()->frontHeadshot == 'FHS_noimage.jpg' || Auth::user()->leftHeadshot == 'LHS_noimage.jpg' || Auth::user()->rightHeadshot == 'RHS_noimage.jpg')
            <div class="card border-warning my-5" style="background: rgba(245, 219, 104, 0.555)">
                <div class="card-body">
                    <p style="color: rgb(209, 98, 7)">Add your headshot photos for better user experience ...</p>
                    <small style="color: rgb(209, 98, 7)">Go to '<a href="/users/{{Auth::user()->id}}/edit" class="stretched-link">Edit Profile</a>' page to add your headshots.</small>
                </div>
            </div>
        @endif
    @endif

    <!--Administrators' Dashboard-->
    <div class="row justify-content-center">
        <!--New Requests Section-->
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">New Requests</div>
                <div class="card-body">
                    @if(count($requests) > 0)
                        <div class="table table-hover">
                            @foreach ($requests as $request)
                                <div class="tr">
                                    <div class="well">
                                        <p><b>{{$request->requestingUserName}}</b> wants to contact you.</p>
                                        Do you want to give your contact info?
                                        {!! Form::open(['action' => ['DashboardController@acceptRequest', $request->requestingUserID], 'method' => 'POST']) !!}
                                            {{ Form::submit('Accept', ['class' => 'btn btn-sm btn-outline-success float-left']) }}
                                        {!! Form::close() !!}

                                        {!! Form::open(['action' => ['DashboardController@rejectRequest', $request->requestingUserID], 'method' => 'POST']) !!}
                                            {{ Form::submit('Decline', ['class' => 'btn btn-sm btn-outline-warning inline float-right']) }}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                        @endforeach
                        </div>
                    @else
                        <div class="well">
                            You do not have any contact requests yet.
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <!--Posted Articles Section-->
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Your Articles</div>

                <div class="card-body">
                    @if(count($articles) > 0)
                        <table class="table table-striped table-hover table-responsive-md">
                            <tr>
                                <th>Post</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach ($articles as $article)
                                <tr>
                                    <td>{{$article->title}}</td>
                                    <td>
                                        <a href="/articles/{{$article->id}}/edit" class="btn btn-outline-secondary">Edit</a>
                                    </td>
                                    <td>
                                        {!! Form::open(['action' => ['ArticlesController@destroy', $article->id], 'method' => 'POST', 'class' => 'inline pull-right float-right mr-md-5']) !!}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            {{ Form::submit('Delete', ['class' => 'btn btn-danger float-right']) }}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <a href="/articles/create" class="btn btn-outline-primary btn-sm float-right">Add a New Article</a>
                    @else
                        <p>You have not posted any articles yet.</p>
                        <a href="/articles/create" class="btn btn-outline-primary btn-sm float-right">Add a New Article</a>
                    @endif

                </div>
            </div>
        </div>

        <!--Upcoming Events Section-->
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Upcoming Events</div>

                <div class="card-body">
                    @if (count($upcomingEvents) > 0)
                        @foreach ($upcomingEvents as $event)
                            <table class="table table-hover">
                                <tr>
                                    <h6><b>{{ $event->title }}</b></h6>
                                    <small>{{ $event->startDate }}, {{ date('h:ia', strtotime($event->startTime)) }} - {{ $event->endDate }}, {{ date('h:ia', strtotime($event->endTime)) }}</small>
                                    @if (Auth::user()->id == $event->userID || Auth::user()->previllage == 'Admin')
                                        <div class="row">
                                            <div class="col-md-6 float-md-left">
                                                <a href="/events/{{$event->id}}/edit" class="btn btn-outline-secondary btn-sm float-md-left">Edit</a>
                                            </div>
                                            <div class="col-md-6 float-md-right">
                                                {!! Form::open(['action' => ['EventsController@destroy', $event->id], 'method' => 'POST', 'class' => 'inline pull-right float-right mr-md-5']) !!}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm float-md-right']) }}
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    @endif
                                </tr>
                            </table>
                        @endforeach
                        @if (Auth::user()->previllage == 'Admin')
                            <a href="/events/create" class="btn btn-outline-primary btn-sm float-right">Add a New Event</a>
                        @endif
                    @else
                        <p>There are no upcoming events.</p>
                        @if (Auth::user()->previllage == 'Admin')
                            <a href="/events/create" class="btn btn-outline-primary btn-sm float-right">Add a New Event</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-start my-md-4">
        <!--New Requests Section-->
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Accepted Requests</div>
                <div class="card-body">
                    @if(count($accepted) > 0)
                        <div class="table table-hover">
                            @foreach ($accepted as $item)
                                <div class="tr">
                                    <div class="well">
                                        <p><b>{{$item->fName}} {{$item->lName}}</b> has accepted your request.</p>
                                        <br>
                                        {{$item->email}}
                                    </div>
                                </div>
                        @endforeach
                        </div>
                    @else
                        <div class="well">
                            You do not have any accepted contacts yet.
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Unarchived Events</div>

                <div class="card-body">
                    @if (count($unarchivedEvents) > 0)
                        @foreach ($unarchivedEvents as $event)
                            <table class="table table-hover">
                                <tr>
                                    <h6><b>{{ $event->title }}</b></h6>
                                    <small>Held on: {{ $event->startDate }}, {{ date('h:ia', strtotime($event->startTime)) }} - {{ $event->endDate }}, {{ date('h:ia', strtotime($event->endTime)) }}</small>
                                    @if (Auth::user()->id == $event->userID || Auth::user()->previllage == 'Admin')
                                        <div class="row">
                                            <div class="col-md-6 float-md-left">
                                                <a href="/events/archive/{{$event->id}}/edit" class="btn btn-outline-secondary btn-sm float-md-left">Archive Event</a>
                                            </div>
                                            {{-- <div class="col-md-6 float-md-right">
                                                {!! Form::open(['action' => ['EventsController@destroy', $event->id], 'method' => 'POST', 'class' => 'inline pull-right float-right mr-md-5']) !!}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-sm float-md-right']) }}
                                                {!! Form::close() !!}
                                            </div> --}}
                                        </div>
                                    @endif
                                </tr>
                            </table>
                        @endforeach
                    @else
                        <p>There are no upcoming events.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
