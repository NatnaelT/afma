@extends('layouts.app')

@section('content')
    <!--Events Page Cover-->
    {{-- <div class="article-jumbotron">
        <div class="mx-5 py-3">
            <h1 class="mb-md-4">Events</h1>
            <div class="ml-md-3">
                <h5>Filter by:</h5>
                <div class="row align-items-lg-end">
                    <div class="col-md-3 justify-content-md-start">
                        <p class="lead font-weight-bolder">Category</p>
                        <small>Selection drop-down form goes here.</small>
                    </div>
                    <div class="col-md-3 justify-content-md-start">
                        <p class="lead font-weight-bolder">Date</p>
                        <small>Selection drop-down form goes here.</small>
                    </div>
                    <div class="col-md-6 col-md-offset-3 justify-content-md-end">
                        <div class="row justify-content-md-end mr-4">
                            <div class="col-sm-8">
                                <form action="" class="search-form">
                                    <div class="form-group has-feedback">
                                        <label for="search" class="sr-only">Search</label>
                                        <input type="text" class="form-control" name="search" id="search" placeholder="search">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="row mt-5 justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    Upcoming Events
                </div>
                <div class="cad-body">
                    <div class="mx-md-5 px-md-5 my-md-5 py-md-4">
                        @if (count($upcomingEvents) > 0)
                            @foreach ($upcomingEvents as $upcomingEvent)
                                <div class="well">
                                    <div class="mb-md-5 pb-md-2">
                                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                                            <div class="col-8 d-none d-lg-block ml-md-4 pr-md-2 my-md-3">
                                                <h3 class="mb-0 font-weight-bold">{{$upcomingEvent->title}}</h3>
                                                <div class="mb-1 text-muted mt-md-n2 mb-md-2 ml-md-2">{{ $upcomingEvent->startDate }}, {{ date('h:ia', strtotime($upcomingEvent->startTime)) }}</div>
                                                <p class="card-text mb-auto">{{$upcomingEvent->description}}</p>
                                            </div>

                                            <div class="col d-flex flex-column position-static">
                                                <img style="width: 100% !important" src="/storage/eventCoverImages/{{$upcomingEvent->coverImage}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <!--Pagination-->
                            <div class="row text-center">
                                {{$upcomingEvents->links()}}
                            </div>

                        @else
                            <div class="badge-warning">
                                <p>There are no upcoming events.</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    Past Events
                </div>
                <div class="card-body">
                    <div class="mx-md-5 px-md-5 my-md-5 py-md-4">
                        @if (count($expiredEvents) > 0)
                            @foreach ($expiredEvents as $expiredEvent)
                                <div class="well">
                                    <div class="mb-md-5 pb-md-2">
                                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                                            <div class="col-8 d-none d-lg-block ml-md-4 pr-md-2 my-md-3">
                                                <h3 class="mb-0 font-weight-bold">{{$expiredEvent->title}}</h3>
                                                <div class="mb-1 text-muted mt-md-n2 mb-md-2 ml-md-2">{{$expiredEvent->startDate}}, {{$expiredEvent->startTime}}</div>
                                                <p class="card-text mb-auto">{{$expiredEvent->description}}</p>
                                            </div>

                                            <div class="col d-flex flex-column position-static">
                                                {{-- <img style="width: 100% !important" src="/storage/coverImages/{{$article->coverImage}}"> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <!--Pagination-->
                            <div class="row text-center">
                                {{$upcomingEvents->links()}}
                            </div>

                        @else
                            <div class="badge-warning">
                                <p>No event has passed. You can still join in ... </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
