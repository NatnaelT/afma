@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1>Add Event</h1>
        <div class="container">
            {!! Form::open(['action' => 'EventsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!-- Event Title Input Field -->
                <div class="form-group">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Event name goes here ...'])}}
                </div>

                <!-- Event Description Input Field -->
                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::textarea('description', '', ['class' => 'form-control', 'placeholder' => 'Brief statement about the event goes here ...']) }}
                </div>

                <!-- Event Cover Image Input Field -->
                <div class="form-group">
                    <div class="custom-file">
                        {{ Form::label('coverImage', "Event's Cover Image") }}
                        {{ Form::file('coverImage', ['class' => 'form-control, custom-file-input']) }}
                    </div>
                </div>

                <!-- Event Timespan Check -->
                <div class="form-group">
                    {{ Form::label('isAllDay', 'Is All Day') }}
                    {{ Form::checkbox('isAllDay', 'isAllDay', true) }}
                </div>

                <!-- Event Timespan -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="startDate" class="col-md-4">Start Date</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="startDate">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="endDate" class="col-md-4">End Date</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="endDate">
                            </div>
                        </div>
                    </div>
                    {{-- @if (isAllDay == true) --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="startTime" class="col-md-4">Start Time</label>
                                <div class="col-md-6">
                                    <input type="time" class="form-control" name="startTime">
                                </div>
                            </div>

                            <div>
                                <label for="endTime" class="col-md-4">End Time</label>
                                <div class="col-md-6">
                                    <input type="time" class="form-control" name="endTime">
                                </div>

                            </div>
                        </div>
                    {{-- @endif --}}
                </div>
                {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
