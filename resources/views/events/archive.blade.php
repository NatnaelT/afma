@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1>Archive Event</h1>
        <div class="container">
            {!! Form::open(['action' => ['EventsController@archiveEvent', $event->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!-- Event Title Input Field -->
                <div class="form-group">
                    {{ Form::label('title', 'Event Name') }}
                    {{ Form::text('title', $event->title, ['class' => 'form-control', 'placeholder' => 'Event name goes here ...', 'disabled', 'readonly'])}}
                </div>

                <!-- Event Description Input Field -->
                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::textarea('description', $event->description, ['class' => 'form-control', 'placeholder' => 'Brief statement about the event goes here ...', 'disabled', 'readonly']) }}
                </div>

                <!-- Event Photo Gallery Input Field -->
                <div class="input-group control-group increment" >
                    <input type="file" name="filename[]" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                    </div>
                </div>
                <div class="clone hide">
                    <div class="control-group input-group" style="margin-top:10px">
                        <input type="file" name="filename[]" class="form-control">
                        <div class="input-group-btn">
                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                        </div>
                    </div>
                </div>

                {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
            {!! Form::close() !!}
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function() {
            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });
        });

    </script>
@endsection
