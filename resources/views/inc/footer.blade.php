{{-- Footer section of AFMA Website layout --}}

<footer id="footer" class="footer navbar-dark bg-dark">
    <div class="main-footer widgets-dark typo-light my-5 py-5">
        <div class="container">
            <div class="row">

                <!--About AFMA-->
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="widget subscribe no-box">
                        <h5 class="widget-title font-weight-bold my-0 py-0">Alatinos Ethiopian Filmmakers' Association<span></span></h5>
                        <hr class="footer-horizontal-break mt-0 pt-0 ml-0">
                        <p>About the company, little discription will goes here.. </p>
                    </div>
                </div>

                <!--Location-->
                <div class="col-xs-12 col-sm-6 col-md-3 mb-0 mt-3">
                    <h5 class="widget-title my-0 py-0">Location<span></span></h5>
                    <hr class="footer-horizontal-break mt-0 pt-0 w-50 ml-0">
                    <div class="card card-cascade narrower">
                        <div class="card-body card-body-cascade text-center mx-0 my-0 px-0 py-0 mb-0 pb-0">

                            <!--Google Maps-->
                            <div div id="map-container-google-9" class="z-depth-1-half map-container-5" style="height: 200px">

                                <!--Embedded Map Tag-->
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.344953741935!2d38.747410214276556!3d9.032263391426033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x164b85f721e32cdd%3A0xa97917b43251d08a!2sRussian%20Center%20for%20Science%20and%20Culture!5e0!3m2!1sen!2set!4v1584728767976!5m2!1sen!2set" width="400" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Affiliate Links-->
                <div class="col-xs-12 col-sm-6 col-md-3 mt-3">
                    <div class="widget no-box">
                        <h5 class="widget-title my-0 py-0">Affiliate Links<span></span></h5>
                        <hr class="footer-horizontal-break mt-0 pt-0 w-50 ml-0">
                        <ul class="thumbnail-widget list-unstyled">
                            <li>
                                <div class="thumb-content"><a href="#.">Get Started</a></div>
                            </li>
                            <li>
                                <div class="thumb-content"><a href="#.">Top Leaders</a></div>
                            </li>
                            <li>
                                <div class="thumb-content"><a href="#.">Success Stories</a></div>
                            </li>
                            <li>
                                <div class="thumb-content"><a href="#.">Event/Tickets</a></div>
                            </li>
                            <li>
                                <div class="thumb-content"><a href="#.">News</a></div>
                            </li>
                            <li>
                                <div class="thumb-content"><a href="#.">Lifestyle</a></div>
                            </li>
                            <li>
                                <div class="thumb-content"><a href="#.">About</a></div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!--Social-media Handles-->
                <div class="col-xs-12 col-sm-6 col-md-3 mt-3">
                    <div class="widget no-box">
                        <h5 class="widget-title my-0 py-0">Connect with Us<span></span></h5>
                        <hr class="footer-horizontal-break mt-0 pt-0 w-75 ml-0">
                        <p><a href="mailto:info@domain.com" title="glorythemes">info@domain.com</a></p>
                        <ul class="social-footer list-inline ml-3">
                            <li class="list-inline-item"><a href="https://www.facebook.com/" target="_blank" title="Facebook" class="fa fa-facebook button button-primary"></a></li>
                            <li class="list-inline-item"><a href="https://twitter.com" target="_blank" title="Twitter" class="fa fa-twitter button button-primary"></a></li>
                            <li class="list-inline-item"><a href="https://www.instagram.com/" target="_blank" title="Instagram" class="fa fa-instagram button button-primary"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Copyright Statement-->
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <hr class="footer-horizontal-break w-75 text-center">
                <div class="col-md-12 text-center mb-0 pb-0">
                    <p class="text-small">Copyright. <i class="font-weight-bold">Alatinos Ethiopian Filmmakers' Association</i> © 2020. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
