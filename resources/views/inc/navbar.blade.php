{{-- Navigation section of AFMA Website layout --}}

<header role="banner">

    <!--AFMA Logo - Scrollable-->
    {{-- <img id="logo-main" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/32877/logo-thing.png" width="200" alt="AFMA main logo"> --}}


    <!--Navigation Menu-->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm sticky-top">
        <div class="container">
            <img src="/storage/assets/logo.png" alt="AFMA Main Logo" width="25" class="mr-3">
            <a class="navbar-brand" href="{{url('/')}}">{{config('app.name', 'AFMA')}}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/articles">Articles</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/events">Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/users">Members</a>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->fName }} {{ Auth::user()->lName }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{url('dashboard')}}"> My Account </a>

                                <a href="/users/{{ Auth::user()->id }}/edit" class="dropdown-item"> Edit Profile </a>
                            </div>
                        </li>
                    @endguest
                    <li class="nav-item dropdown">
                        <form method="POST" id="locale_form" action="{{ route('locale') }}">
                            @csrf
                            <input type="hidden" id="locale_name" name="locale" />
                        </form>
                        <a id="navbarDropdownlocale" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if (App::currentLocale() == "amh")
                                <img class="img-fluid" src="{{ url('images/am.png') }}"/>
                                አማርኛ
                            @else
                                <img class="img-fluid" src="{{ url('images/en.png') }}"/>
                                English
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-menu-right float align-self-center" aria-labelledby="navbarDropdownlocale" style="min-width: 6rem !important; max-width: 10rem !important;">
                            <a class="dropdown-item" onclick="changelocale('en');">
                                <img class="img-fluid" src="{{ url('images/en.png') }}"/>
                                English
                            </a>
                            <a class="dropdown-item" onclick="changelocale('amh');">
                                <img class="img-fluid" src="{{ url('images/am.png') }}"/>
                                አማርኛ
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>


{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav> --}}
