@extends('layouts.app')

@section('content')
    <!-- Members Page Cover -->
    {{-- <div class="article-jumbotron">
        <div class="mx-5 py-3">
            <h1 class="mb-md-4">Members</h1>
            <div class="ml-md-3">
                <h5>Filter by:</h5>
                <div class="row align-items-lg-end">
                    <div class="col-md-3 justify-content-md-start">
                        <form action="{{ route('users.index') }}">
                            <select name="category" id="category" class="form-control form-control-sm" value="{{ $category }}" (change)="{{ route('users.index') }}">
                                <option value="">Category</option>
                                <option value="actor">Actor</option>
                                <option value="director">Director</option>
                                <option value="producer">Producer</option>
                                <option value="camera">Camera Operator</option>
                            </select>
                        </form>

                    </div>
                    <div class="col-md-3 justify-content-md-start">
                        <select name="filter-sex" id="filter_sex" class="form-control form-control-sm" value="{{ $sex }}" (change)="{{ route('users.index') }}">
                            <option value="">Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div> --}}
                    {{--<div class="col-md-3">
                        <div class="form-row form-group">
                            <button type="button" name="filter" id="filter" class="col-md-6 btn btn-primary">Filter</button>
                            <button type="button" name="reset" id="reset" class="col-md-6 btn btn-default">Reset</button>
                        </div>
                    </div> --}}
                    {{-- <div class="col-md-6 align-self-md-end">
                        <div class="ml-4 align-self-md-end">
                            <form action="{{ route('users.index') }}">
                                <div class="row align-self-md-end">
                                    <div class="col-md-9 form-group align-self-md-end">
                                        <label for="search_string" class="">Search</label>
                                        <input type="search" name="search_string" value="{{ $search_string }}" class="form-control form-control-sm" placeholder="Search with a keyword here ...">
                                    </div>
                                    <div class="col-md-3 align-self-md-center align-self-md-end">
                                        <button type="submit" class="btn btn-sm btn-info">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Members are in chronological order, with the latest showing first -->
    <div class="m-5 px-5 py-4">
        @if(count($users) > 0 && isset($users))
            @foreach ($users as $user)
                <div class="well">
                    <div class="mb-md-5 pb-md-2">
                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                            <div class="col-8 d-none d-lg-block ml-md-4 pr-md-2 my-md-3">
                                <h4 class="font-weight-bold">{{$user->fName}} {{$user->lName}}</h4>
                                {{-- {{ count($userCredentials) }} --}}
                                {{-- {{ $userCredentials }} --}}
                                {{-- {{ $users[0] }} --}}
                                {{-- {{ $userCredentials[0] }} --}}
                                @foreach ($userCredentials as $userCredential)
                                    @if ($userCredential->userID == $user->id)
                                    <p class="card-text mb-auto">Talent: {{$userCredential->fieldOfExpertise}}</p>
                                    @endif
                                @endforeach
                                <div class="mt-2">
                                    <a href="/users/{{$user->id}}" class="stretched-link">View Detail</a>
                                </div>
                                {{-- <div class="mt-3 float-md-right">
                                    @if (Auth::user()->previllage == 'Admin')
                                        <div class="container">
                                            {!! Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'inline pull-right float-right mr-md-4']) !!}
                                                {{ Form::hidden('_method', 'DELETE') }}
                                                {{ Form::submit('Delete', ['class' => 'btn btn-danger float-right', 'onClick' => "return confirm('Are you sure?')"]) }}
                                            {!! Form::close() !!}
                                        </div>
                                    @endif
                                </div> --}}
                            </div>

                            <div class="col d-flex flex-column position-static">
                                <img style="min-width: 30% !important; max-width: 40% !important;" class="rounded mr-0 float-right" src="/storage/userPPs/{{$user->userProfilePic}}">
                                {{-- <svg class="bd-placeholder-img" width="361" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg> --}}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <!--Pagination-->
            <div class="row text-center">
                {{$users->links()}}
            </div>

            @else
            <div class="badge-warning">
                <p>No users found.</p>
            </div>
        @endif
    </div>
@endsection
