@extends('layouts.app')

@section('content')

    <div class="row mt-5 justify-content-center">
        <img style="width:150px !important; height:150px !important; float:center !important; border-radius: 50% !important; margin-right: auto !important; margin-left: auto !important;" src="/storage/userPPs/{{$user->userProfilePic}}" alt="{{$user->fName}} {{$user->lName}}'s Profile Picture">
    </div>

    <div class="row my-5 justify-content-center">
        <h3> {{ $user->fName }} {{ $user->lName }} </h3>
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-auto">
                <div class="card">
                    <img class="card-img" src="/storage/userHeadshots/{{$user->leftHeadshot}}" style="width: 150px; height:200px; border-radius:20px;" alt="Left Headshot">
                </div>
            </div>

            <div class="col-md-auto">
                <div class="card">
                    <img class="card-img" src="/storage/userHeadshots/{{$user->frontHeadshot}}" style="width: 150px; height:200px; border-radius:20px;" alt="Front Headshot">
                </div>
            </div>

            <div class="col-md-auto">
                <div class="card">
                    <img class="card-img" src="/storage/userHeadshots/{{$user->rightHeadshot}}" style="width: 150px; height:200px; border-radius:20px;" alt="Right Headshot">
                </div>
            </div>
        </div>
    </div>

    <div class="container justify-content-center">
        <p class="">Age: {{$age}} </p>
        <p class="">Gender: {{$user->sex}}</p>
        <p class="">Occupation: {{$userCredential->occupation}}</p>
        <p class="">Field of Expertise: {{$userCredential->fieldOfExpertise}}</p>
        <p class="">Field of Interest: {{$userCredential->fieldOfInterest}}</p>
    </div>
    <hr>
    @if(!Auth::guest())
        <div class="container">
            {!! Form::open(['action' => ['DashboardController@createRequest', $user->id], 'method' => 'POST']) !!}
                {{ Form::submit('Request Contact Info', ['class' => 'btn btn-sm btn-primary']) }}
            {!! Form::close() !!}
            {{-- <form action="{{ route('dashboard.createRequest', $user->id) }}" method="POST">
                <button class="btn btn-sm btn-primary" type="submit">Request Contact Info</button>
            </form> --}}
        </div>
        @if(Auth::user()->id == $user->id)
            <div class="container">
                <a href="/users/{{$user->id}}/edit" class="btn btn-outline-secondary">Edit</a>

                {!! Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'inline pull-right float-right mr-md-5']) !!}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger float-right', 'onClick' => "return confirm('Are you sure?')"]) }}
                {!! Form::close() !!}
            </div>
        @endif

        @if (Auth::user()->previllage == 'Admin')
            <div class="container">
                {!! Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST', 'class' => '']) !!}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger ml-md-auto mr-md-5', 'onClick' => "return confirm('Are you sure?')"]) }}
                {!! Form::close() !!}
            </div>
        @endif
    @endif
@endsection
