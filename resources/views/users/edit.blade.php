@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1>Edit Profile</h1>
        <div class="container">
            {!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!-- User Fullname Input Field -->
                <div class="form-group">
                    {{ Form::label('fName', 'First name') }}
                    {{ Form::text('fName', $user->fName, ['class' => 'form-control', 'placeholder' => 'Your First Name']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('lName', 'Last name') }}
                    {{ Form::text('lName', $user->lName, ['class' => 'form-control', 'placeholder' => 'Your Last Name']) }}
                </div>

                <!-- User Birth Date Input Field -->
                <div class="form-group">
                    {{ Form::label('bDate', 'Birth date') }}
                    {{ Form::date('bDate', $user->bDate, ['class' => 'form-control, date', 'type' => 'date', 'placeholder' => 'Your Birthdate']) }}
                </div>

                <!-- User Sex -->
                <div class="form-group">
                    {{ Form::label('sex', 'Gender') }}
                    {{ Form::text('sex', $user->sex, ['class' => 'form-control', 'placeholder' => 'Your Sexual Orientation']) }}
                </div>

                <!-- User Email -->
                <div class="form-group">
                    {{ Form::label('emal', 'Email') }}
                    {{ Form::text('email', $user->email, ['class' => 'form-control', 'type' => 'email', 'placeholder' => 'Your Email']) }}
                </div>

                <!-- User Profile Picture -->
                <div class="form-group">
                    <div class="custom-file">
                        {{ Form::label('userProfilePic', 'Profile Picture') }}
                        {{ Form::file('userProfilePic'), $user->userProfilePic }}
                    </div>
                </div>

                <!-- User Headshots -->
                <div class="form-group">
                    {{-- <div class="custom-file"> --}}
                        {{ Form::label('frontHeadshot', 'Frontal Headshot') }}
                        {{ Form::file('frontHeadshot'), $user->frontHeadshot }}
                    {{-- </div> --}}
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        {{ Form::label('leftHeadshot', 'Left Headshot') }}
                        {{ Form::file('leftHeadshot') }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        {{ Form::label('rightHeadshot', 'Right Headshot') }}
                        {{ Form::file('rightHeadshot') }}
                    </div>
                </div>

                <!-- User Educational Background -->
                <div class="form-group">
                    {{ Form::label('educationalBackground', 'Educational Background') }}
                    {{ Form::text('educationalBackground', $userCredential->educationalBackground, ['class' => 'form-control', 'placeholder' => 'Your Educational Background']) }}
                </div>

                <!-- User Occupation -->
                <div class="form-group">
                    {{ Form::label('occupation', 'Occupation') }}
                    {{ Form::text('occupation', $userCredential->occupation, ['class' => 'form-control', 'placeholder' => 'Your Occupation']) }}
                </div>

                <!-- User Field of Expertise -->
                <div class="form-group">
                    {{ Form::label('fieldOfExpertise', 'Field of Expertise') }}
                    {{ Form::text('fieldOfExpertise', $userCredential->fieldOfExpertise, ['class' => 'form-control', 'placeholder' => 'Your Field of Expertise']) }}
                </div>

                <!-- User Field of Interest -->
                <div class="form-group">
                    {{ Form::label('fieldOfInterest', 'Field of Interest') }}
                    {{ Form::text('fieldOfInterest', $userCredential->fieldOfInterest, ['class' => 'form-control', 'placeholder' => 'Your Field of Interest']) }}
                </div>

                <!--Spoof a PUT request from a POST method.-->
                {{Form::hidden('_method', 'PUT')}}

                {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
