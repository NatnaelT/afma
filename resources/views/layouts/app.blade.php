<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}"/>

        <!-- Yajra Datatable -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> --}}
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
        {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}

        <!-- Jquery UI css -->
        <link rel="stylesheet" href="//code/jquery.com/ui/1.12.1/themes/base/jquery-ui.css">



    </head>
    <body>
        <div id="app">
            @include('inc.navbar')

            @include('inc.messages')

            @yield('content')

            @include('inc.footer')
        </div>

        <!-- Scripts -->
        <!-- Script for ckeditor 4 -->
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <script>
            CKEDITOR.replace( 'article-ckeditor' );
            function changelocale(locale) {
                document.getElementById('locale_name').value = locale;
                $('#locale_form').submit();
            }

            /* To display the values in the textarea in console.*/
            // var editor = CKEDITOR.replace( 'article-ckeditor' );
            // editor.on('change', function( evt ){
            //     console.log(evt.editor.getData());
            // });
        </script>

        <script src="{{ asset('js/app.js') }}"></script>

        {{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> --}}

        <!-- Slick -->
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="{{asset('slick/slick.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/slickSlider.js')}}"></script>
    </body>
</html>
