<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table = 'events';
    protected $fillable = ['title', 'startDate', 'endDate', 'coverImage'];

    public function user(){
        return $this->belongsTo('App\User', 'userID');
    }

    public function eventPictures(){
        return $this->hasMany('App\EventPicture', 'eventID');
    }
}
