<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    use HasFactory;
    protected $fillable = ['title', 'description', 'column', 'coverImage', 'imageCourtsy'];

    public function user(){
        return $this->belongsTo('App\User', 'authorID');
    }
}
