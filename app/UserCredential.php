<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCredential extends Model
{
    use HasFactory;
    //
    protected $fillable = [
        'userID', 'educationalBackground', 'occupation', 'fieldOfExpertise', 'fieldOfInterest'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'userID');
    }
}
