<?php

namespace App\Http\Controllers;

use App\Article;
use App\Event;
use App\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    //Returns the landing page for a request
    public function index(){

        $articles = Article::latest()->paginate(1);
        $events = Event::latest()->paginate(1);
        return view('pages.index')->with('articles', $articles)->with('events', $events);
    }
    //Returns the articles page for a request
    public function articles(){
        return view('pages.articles');
    }
    //Returns the events page for a request
    public function events(){
        return view('pages.events');
    }
    //Returns the members (Talents' Catalog) page for a request
    public function members(){
        return view('pages.members');
    }
    //Returns user's page for a specific request given userID. [Needs ID param to be passed as argument to process teh info from database]
    public function myaccount(){
        return view('pages.myaccount');
    }

    public function changelocale(Request $request)
    {
        $locale = $request->input('locale');

        if (in_array($locale, ['en', 'amh'])) {
            $request->session()->put('locale', $locale);
        }

        return redirect()->back();
    }
}
