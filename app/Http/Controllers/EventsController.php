<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventPicture;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redirect,Response;

class EventsController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $today = Carbon::today()->toDateString();
        $nowTime = Carbon::now()->toTimeString();
        $upcomingEvents = Event::where('status', false)->where('endDate', '>=', $today)->where('endTime', '>=', $nowTime)->latest()->paginate(4);
        $expiredEvents = Event::where('status', true)->where('endDate', '<', $today)->where('endTime', '>', $nowTime)->latest()->paginate(3);
        return view('events.events')->with('upcomingEvents', $upcomingEvents)->with('expiredEvents', $expiredEvents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('events.addEvent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'startDate' => 'required'
        ]);

        /* Fetch article's cover image */
        //Handle the image upload
        if ($request -> hasFile('coverImage')){
            //Get filename with extension
            $filenameWithExt = $request->file('coverImage')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just file extension
            $fileExtension = $request->file('coverImage')->getClientOriginalExtension();
            //Set filename to store
            $filenameToStore = $filename.'_'.time().'.'.$fileExtension;
            //Upload Image
            $path = $request->file('coverImage')->storeAs('public/eventCoverImages', $filenameToStore);
            //Storage::disk('coverImages')->put($filenameToStore, File::get($request->file('coverImage')));
        }
        else{
            $filenameToStore = 'noimage.jpg';
        }

        $event = new Event;

        $event->title = $request->input('title');
        $event->description = $request->input('description');
        $event->isAllDay = $request->input('isAllDay');
        $event->startDate = $request->input('startDate');
        $event->startTime = $request->input('startTime');
        $event->endDate = $request->input('endDate');
        $event->endTime = $request->input('endTime');
        $event->coverImage = $filenameToStore;
        $event->status = false;
        $event->userID = auth()->user()->id;

        $event->save();

        return redirect('/events')->with('success', 'Event Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $event = Event::find($id);
        return view('events.show')->with('event', $event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event = Event::find($id);

        if(auth()->user()->id !== $event->userID){
            return redirect('/events')->with('error', 'Unauthorized Access');
        }

        return view('events.edit')->with('event', $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'startDate' => 'required'
        ]);

        /* Fetch article's cover image */
        //Handle the image upload
        if ($request -> hasFile('coverImage')){
            //Get filename with extension
            $filenameWithExt = $request->file('coverImage')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just file extension
            $fileExtension = $request->file('coverImage')->getClientOriginalExtension();
            //Set filename to store
            $filenameToStore = $filename.'_'.time().'.'.$fileExtension;
            //Upload Image
            $path = $request->file('coverImage')->storeAs('public/eventCoverImages', $filenameToStore);
            //Storage::disk('coverImages')->put($filenameToStore, File::get($request->file('coverImage')));
        }
        else{
            $filenameToStore = 'noimage.jpg';
        }

        $event = Event::find($id);

        $event->title = $request->input('title');
        $event->description = $request->input('description');
        $event->isAllDay = $request->input('isAllDay');
        $event->startDate = $request->input('startDate');
        $event->startTime = $request->input('startTime');
        $event->endDate = $request->input('endDate');
        $event->endTime = $request->input('endTime');
        $event->coverImage = $filenameToStore;
        $event->userID = auth()->user()->id;

        $event->save();

        return redirect('/events')->with('success', 'Event Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $event = Event::find($id);

        if(auth()->user()->id !== $event->userID){
            return redirect('/events')->with('error', 'Unauthorized Access');
        }

        $event->delete();
        return redirect('/events')->with('success', 'Event Deleted');
    }

    public function archiveEventShow($id)
    {
        //
        $event = Event::find($id);

        if(auth()->user()->id !== $event->userID || auth()->user()->previllage !== 'Admin'){
            return redirect('/events')->with('error', 'Unauthorized Access');
        }

        return view('events.archive')->with('event', $event);
    }

    public function archiveEvent(Request $request, $id)
    {
        //
        $this->validate($request, [
            'filename' => 'required',
            'filename.*' => 'image|mimes:png,jpg,jpeg|max:4096',
        ]);

        if($request -> hasFile('filename')){
            foreach($request->file('filename') as $image){
                //Get filename with extension
                $filenameWithExt = $image->getClientOriginalName();
                //Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                //Get just file extension
                $fileExtension = $image->getClientOriginalExtension();
                //Set filename to store
                $filenameToStore = $filename.'_'.time().'.'.$fileExtension;
                //Upload Image
                $path = $image->storeAs('public/eventGallery', $filenameToStore);
                $data[] = $filenameToStore;
            }
        }
        else{
            $filenameToStore = 'noimage.jpg';
            $data[] = $filenameToStore;
        }

        $event = Event::find($id);
        $eventGallery = new EventPicture;
        $event->status = true;
        $event->save();

        foreach($data as $item){
            $eventGallery->picturePath = $filenameToStore;
            $eventGallery->eventID = $event->id;
            $eventGallery->save();
        }

        return redirect('/dashboard')->with('success', 'Event Archived');
    }

    protected function saveImage(Request $request, $id){

    }
}
