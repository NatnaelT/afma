<?php

namespace App\Http\Controllers;

use App\User;
use App\UserCredential;
use App\UserReference;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Redirect,Response,DB,Config;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Type\Integer;

class UsersController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // if($request()->ajax())
        // {
        //     if(!empty($request->filter_sex))
        //     {
        //         $data = User::where('sex', $request->filter_sex)->get();
        //     }
        //     else if(!empty($request->filter_category))
        //     {
        //         $data = User::where('previllage', $request->filter_category)->get();
        //     }
        //     else
        //     {
        //         $data = User::filter($request)->latest()->paginate(10);
        //     }
        //     return $data;
        // }

        $search_string = null;
        $category = null;
        $age = null;
        $sex = null;

        if($request->has('search_string'))
        {
            $search_string = $request->query('search_string');
            $users = User::search($search_string)->latest()->paginate(10);
        }
        if($request->has('category'))
        {
            $category = $request->query('category');
            $users = User::search($category)->latest()->paginate(10);
        }
        if($request->has('age'))
        {
            $age = $request->query('age');
            $users = User::search($age)->latest()->paginate(10);
        }
        if($request->has('sex'))
        {
            $sex = $request->query('sex');
            $users = User::search($sex)->latest()->paginate(10);
        }
        else
        {
            $users = User::latest()->paginate(10);
        }

        // $userIds = array('');

        // for($i = 0; $i < sizeof($users); $i++){
        //     $userIds = array_push($userIds, $users[$i]->id);
        // }

        // $userCredentials = array('' => new UserCredential, );

        // for($i = 0; $i < sizeof($users); $i++){
            $userCredentials = UserCredential::whereIn('userID', User::latest()->paginate(10)->pluck('id'))->orderBy('id', 'desc')->get();
        // }
        return view('users.users')->with('users', $users)
                                  ->with('userCredentials', $userCredentials)
                                  ->with('search_string', $search_string)
                                  ->with('category', $category)
                                  ->with('age', $age)
                                  ->with('sex', $sex);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);
        $userCredential = UserCredential::where('userID', $id)->first();
        $userReference = UserReference::where('userID', $id)->first();

        $BoD = $user->bDate;
        $BoD = new DateTime($BoD);
        $dif = $BoD->diff(new DateTime());
        $age = $dif->y;
        return view('users.show')->with('user', $user)->with('userCredential', $userCredential)->with('userReference', $userReference)->with('age', $age);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $userCredential = UserCredential::where('userID', $id)->first();
        $userReference = UserReference::where('userID', $id)->first();

        //Checking for the correct user
        if(auth()->user()->id != $user->id){
            return redirect('/users')->with('error', 'Unauthorized Access');
        }

        $userData = [
            $user,
            $userCredential,
            $userReference
        ];

        return view('users.edit')->with('user', $user)->with('userCredential', $userCredential)->with('userReference', $userReference);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            //Set validation restrictions for the input parameters
            'fName' => 'required',
            'lName' => 'required',
            'bDate' => 'required',
            'sex' => 'required',
            'userProfilePic' => 'image|nullable|max:5000|mimes:png,jpg,jpeg,bmp,svg',
            'frontHeadshot' => 'required|image|max:5000|mimes:png,jpg,jpeg,bmp,svg',
            'leftHeadshot' => 'image|nullable|max:5000|mimes:png,jpg,jpeg,bmp,svg',
            'rightHeadshot' => 'image|nullable|max:5000|mimes:png,jpg,jpeg,bmp,svg',
            'email' => 'required',
            // 'password' => 'required',
            'educationalBackground' => 'required',
            'occupation' => 'required',
            'fieldOfExpertise' => 'required',
            'fieldOfInterest' => 'required',
        ]);

        /* Fetch user's profile picture */
        // Handle the profile picture image upload
        if($request -> hasFile('userProfilePic')){

            $upp = $request->file('userProfilePic');
            $filenameWithExt = $upp->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $fileExtension = $upp->getClientOriginalExtension();
            $UPCfilenameToStore = 'UPC'.'_'.$filename.'_'.time().'.'.$fileExtension;
            Image::make($upp)->resize(300, 300)->save(public_path('/storage/userPPs/').$UPCfilenameToStore);
        }
        else{
            $UPCfilenameToStore = 'UPC_noimage.jpg';
        }

        // Handle the headshot images upload
        if($request -> hasFile('frontHeadshot')){
            $fhs = $request->file('frontHeadshot');
            $filenameWithExt = $fhs->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $fileExtension = $fhs->getClientOriginalExtension();
            $FHSfilenameToStore = 'FHS'.'_'.$filename.'_'.time().'.'.$fileExtension;
            Image::make($fhs)->resize(300, 300)->save(public_path('/storage/userHeadshots/').$FHSfilenameToStore);
        }
        else{
            $FHSfilenameToStore = 'FHS_noimage.jpg';
        }

        if($request -> hasFile('leftHeadshot')){
            $lhs = $request->file('leftHeadshot');
            $filenameWithExt = $lhs->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $fileExtension = $lhs->getClientOriginalExtension();
            $LHSfilenameToStore = 'FHS'.'_'.$filename.'_'.time().'.'.$fileExtension;
            Image::make($lhs)->resize(300, 300)->save(public_path('/storage/userHeadshots/').$LHSfilenameToStore);
        }
        else{
            $LHSfilenameToStore = 'LHS_noimage.jpg';
        }

        if($request -> hasFile('rightHeadshot')){
            $rhs = $request->file('rightHeadshot');
            $filenameWithExt = $rhs->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $fileExtension = $rhs->getClientOriginalExtension();
            $RHSfilenameToStore = 'FHS'.'_'.$filename.'_'.time().'.'.$fileExtension;
            Image::make($rhs)->resize(300, 300)->save(public_path('/storage/userHeadshots/').$RHSfilenameToStore);
        }
        else{
            $RHSfilenameToStore = 'RHS_noimage.jpg';
        }

        // Create a user object
        $user = User::find($id);

        // Create a user creadential object
        $userCredential = UserCredential::where('userID', $id)->first();
        $userReference = UserReference::where('userID', $id)->first();

        // Fetch user's first name
        $user->fName = $request->input('fName');

        $user->lName = $request->input('lName');

        $user->bDate = $request->input('bDate');

        $user->sex = $request->input('sex');

        $user->email = $request->input('email');

        $user->userProfilePic = $UPCfilenameToStore;

        $user->frontHeadshot = $FHSfilenameToStore;

        $user->leftHeadshot = $LHSfilenameToStore;

        $user->rightHeadshot = $RHSfilenameToStore;

        $userCredential->educationalBackground = $request->input('educationalBackground');

        $userCredential->occupation = $request->input('occupation');

        $userCredential->fieldOfExpertise = $request->input('fieldOfExpertise');

        $userCredential->fieldOfInterest = $request->input('fieldOfInterest');

        $user->save();

        $userCredential->save();

        $userReference->save();

        return redirect('/dashboard')->with('success', 'Profile Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);

        $userCredential = UserCredential::where('userID', $id)->first();
        $userReference = UserReference::where('userID', $id)->first();

        if(auth()->user()->id !== $user->id || auth()->user()->previllage === 'Admin'){
            return redirect('/home')->with('error', 'Unauthorized Access');
        }

        $user->delete();
        $userCredential->delete();
        $userReference->delete();
        return redirect('/users')->with('success', 'User Deleted');
    }
}
