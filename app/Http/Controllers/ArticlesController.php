<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Redirect,Response,DB,Config;

class ArticlesController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        /* $authorID = auth()->user()->id;
        $author = User::find($authorID); */
        /*, $author->articles */

        $articles = Article::latest()->paginate(4);
        return view('articles.articles')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('articles.addArticle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            //Set validation restrictions for the input parameters
            'title' => 'required',
            'description' => 'required',
            'body' => 'required',
            'coverImage' => 'image|nullable|max:1999'
        ]);

        /* Fetch article's cover image */
        //Handle the image upload
        if ($request -> hasFile('coverImage')){
            //Get filename with extension
            $filenameWithExt = $request->file('coverImage')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just file extension
            $fileExtension = $request->file('coverImage')->getClientOriginalExtension();
            //Set filename to store
            $filenameToStore = $filename.'_'.time().'.'.$fileExtension;
            //Upload Image
            $path = $request->file('coverImage')->storeAs('public/coverImages', $filenameToStore);
            //Storage::disk('coverImages')->put($filenameToStore, File::get($request->file('coverImage')));
        }
        else{
            $filenameToStore = 'noimage.jpg';
        }

        //Create an article
        $article = new Article;

        //Fetch article title
        $article->title = $request->input('title');
        //Fetch article description
        $article->description = $request->input('description');
        //Fetch article body
        $article->column = $request->input('body');
        //Fetch the cover image of article
        $article->coverImage = $filenameToStore;
        //Fetch the cover image's owner's name
        $article->imageCourtsy = $request->input('courtsy');
        //Fetch the author of the article form DB using its ID
        $article->authorID = auth()->user()->id;

        //Store the article to database
        $article->save();

        // print_r($article);

        return redirect('/articles')->with('success', 'Article Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $article = Article::find($id);
        return view('articles.show')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $article = Article::find($id);

        //Checking for the correct user
        if(auth()->user()->id !== $article->authorID){
            return redirect('/articles')->with('error', 'Unauthorized Access');
        }

        return view('articles.edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            //Set validation restrictions for the input parameters
            'title' => 'required',
            'description' => 'required',
            'body' => 'required',
            'coverImage' => 'image|nullable|max:1999'
        ]);

        /* Fetch article's cover image */
        //Handle the image upload
        if ($request -> hasFile('coverImage')){
            //Get filename with extension
            $filenameWithExt = $request->file('coverImage')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just file extension
            $fileExtension = $request->file('coverImage')->getClientOriginalExtension();
            //Set filename to store
            $filenameToStore = $filename.'_'.time().'.'.$fileExtension;
            //Upload Image
            $path = $request->file('coverImage')->storeAs('public/coverImages', $filenameToStore);
            //Storage::disk('coverImages')->put($filenameToStore, File::get($request->file('coverImage')));
        }
        else{
            $filenameToStore = 'noimage.jpg';
        }

        //Create an article
        $article = Article::find($id);

        //Fetch article title
        $article->title = $request->input('title');
        //Fetch article description
        $article->description = $request->input('description');
        //Fetch article body
        $article->column = $request->input('body');
        //Fetch the cover image of article
        $article->coverImage = $filenameToStore;
        //Fetch the cover image's owner's name
        $article->imageCourtsy = $request->input('courtsy');

        //Store the article to database
        $article->save();

        return redirect('/articles')->with('success', 'Article Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Article::find($id);

        //Checking for the correct user
        if(auth()->user()->id !== $article->authorID){
            return redirect('/articles')->with('error', 'Unauthorized Access');
        }

        $article->delete();
        return redirect('/articles')->with('success', 'Article Deleted');
    }
}
