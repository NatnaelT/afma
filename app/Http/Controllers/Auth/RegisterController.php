<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserCredential;
use App\UserReference;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::DASHBOARD;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fName' => ['required', 'string', 'max:255'],
            'lName' => ['required', 'string', 'max:255'],
            'bDate' => ['required'],
            'sex' => ['required', 'string', 'max:6'],
            // 'userProfilePic' => ['required', 'image', 'max:5000', 'mimes:png,jpg,jpeg,bmp,svg'],
            // 'frontHeadshot' => ['sometimes', 'image', 'max:5000', 'mimes:png,jpg,jpeg,bmp,svg'],
            // 'leftHeadshot' => ['sometimes', 'image', 'max:5000', 'mimes:png,jpg,jpeg,bmp,svg'],
            // 'rightHeadshot' => ['sometimes', 'image', 'max:5000', 'mimes:png,jpg,jpeg,bmp,svg'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'educationalBackground' => ['required', 'string'],
            'occupation' => ['required', 'string'],
            'fieldOfExpertise' => ['required', 'string'],
            'fieldOfInterest' => ['required', 'string'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // if (request()->hasFile('userProfilePic')) {
        //     // Saving original image
        //     $upp = Image::make(request()->file('userProfilePic'));
        //     $fullSizePath = 'public/userPPs';
        //     $filenameWithExt = request()->file('userProfilePic')->getClientOriginalName();
        //     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //     $fileExtension = request()->file('userProfilePic')->getClientOriginalExtension();
        //     $UPCfilenameToStore = 'UPC' . '_' . $filename . '_' . time() . '.' . $fileExtension;
        //     $upp->save($fullSizePath . $UPCfilenameToStore);

        //     // // Get the filename with extension
        //     // $filenameWithExt = request()->file('userProfilePic')->getClientOriginalName();
        //     // // Get just the filename
        //     // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //     // // Get just the file extension
        //     // $fileExtension = request()->file('userProfilePic')->getClientOriginalExtension();
        //     // // Set filename to store
        //     // $UPCfilenameToStore = 'UPC'.'_'.$filename.'_'.time().'.'.$fileExtension;
        //     // // Upload file
        //     // $fileUploaded = request()->file('userProfilePic');
        //     // $fileUploaded->move(public_path('/public/storage/userPPs/'), $UPCfilenameToStore);
        // } else {
        //     $UPCfilenameToStore = 'UPC_noimage.jpg';
        // }

        $user =  User::create([
            'fName' => $data['fName'],
            'lName' => $data['lName'],
            'bDate' => $data['bDate'],
            'sex' => $data['sex'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'previllage' => 'Client',
            'pending_approval' => 'pending'
            // 'userProfilePic' => '/userPPs/' . $UPCfilenameToStore,
            // 'frontHeadshot' => $data['frontHeadshot'],
            // 'leftHeadshot' => $data['leftHeadshot'],
            // 'rightHeadshot' => $data['rightHeadshot'],
        ]);

        UserCredential::create([
            'userID' => $user->id,
            'educationalBackground' => $data['educationalBackground'],
            'occupation' => $data['occupation'],
            'fieldOfExpertise' => $data['fieldOfExpertise'],
            'fieldOfInterest' => $data['fieldOfInterest'],
        ]);

        UserReference::create([
            'userID' => $user->id,
        ]);

        // /* Fetch user's profile picture */
        // // Handle the profile picture image upload
        // if (request()->hasFile('userProfilePic')) {
        //     // // Saving original image
        //     // $upp = Image::make(request()->file('userProfilePic'));
        //     // $fullSizePath = 'public/userPPs';
        //     // $filenameWithExt = request()->file('userProfilePic')->getClientOriginalName();
        //     // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //     // $fileExtension = request()->file('userProfilePic')->getClientOriginalExtension();
        //     // $UPCfilenameToStore = 'UPC'.'_'.$filename.'_'.time().'.'.$fileExtension;
        //     // $upp->save($fullSizePath.$UPCfilenameToStore);

        //     //     // Get the filename with extension
        //     //     $filenameWithExt = request()->file('userProfilePic')->getClientOriginalName();
        //     //     // Get just the filename
        //     //     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //     //     // Get just the file extension
        //     //     $fileExtension = request()->file('userProfilePic')->getClientOriginalExtension();
        //     //     // Set filename to store
        //     //     $UPCfilenameToStore = 'UPC'.'_'.$filename.'_'.time().'.'.$fileExtension;
        //     //     // Upload image
        //     //     $path = request()->file('userProfilePic')->storeAs('public/userPPs', $UPCfilenameToStore);
        //     //     $user->create(['userProfilePic', $UPCfilenameToStore]);
        //     //     // Storage::disk('userProfilePics')->put($UPCfilenameToStore, File::get($request->file('userProfilePic')));
        //     // }
        //     // else{
        //     //     $UPCfilenameToStore = 'UPC_noimage.jpg';
        //     // }

        //     // Handle the headshot images upload
        //     if (request()->hasFile('frontHeadshot')) {
        //         // Get the filename with extension
        //         $filenameWithExt = request()->file('frontHeadshot')->getClientOriginalName();
        //         // Get just the filename
        //         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //         // Get just the file extension
        //         $fileExtension = request()->file('frontHeadshot')->getClientOriginalExtension();
        //         // Set filename to store
        //         $FHSfilenameToStore = 'FHS' . '_' . $filename . '_' . time() . '.' . $fileExtension;
        //         // Upload image
        //         $path = request()->file('frontHeadshot')->storeAs('public/userHeadshots', $FHSfilenameToStore);
        //         $user->update(['frontHeadshot', $FHSfilenameToStore]);
        //         // Storage::disk('frontHeadshot')->put($FHSfilenameToStore, File::get($request->file('frontHeadshot')));
        //     } else {
        //         $FHSfilenameToStore = 'FHS_noimage.jpg';
        //     }

        //     if (request()->hasFile('leftHeadshot')) {
        //         // Get the filename with extension
        //         $filenameWithExt = request()->file('leftHeadshot')->getClientOriginalName();
        //         // Get just the filename
        //         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //         // Get just the file extension
        //         $fileExtension = request()->file('leftHeadshot')->getClientOriginalExtension();
        //         // Set filename to store
        //         $LHSfilenameToStore = 'LHS' . '_' . $filename . '_' . time() . '.' . $fileExtension;
        //         // Upload image
        //         $path = request()->file('leftHeadshot')->storeAs('public/userHeadshots', $LHSfilenameToStore);
        //         $user->update(['leftHeadshot', $LHSfilenameToStore]);
        //         // Storage::disk('leftHeadshot')->put($LHSfilenameToStore, File::get($request->file('leftHeadshot')));
        //     } else {
        //         $LHSfilenameToStore = 'LHS_noimage.jpg';
        //     }

        //     if (request()->hasFile('rightHeadshot')) {
        //         // Get the filename with extension
        //         $filenameWithExt = request()->file('rightHeadshot')->getClientOriginalName();
        //         // Get just the filename
        //         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //         // Get just the file extension
        //         $fileExtension = request()->file('rightHeadshot')->getClientOriginalExtension();
        //         // Set filename to store
        //         $RHSfilenameToStore = 'RHS' . '_' . $filename . '_' . time() . '.' . $fileExtension;
        //         // Upload image
        //         $path = request()->file('rightHeadshot')->storeAs('public/userHeadshots', $RHSfilenameToStore);
        //         $user->update(['rightHeadshot', $RHSfilenameToStore]);
        //         // Storage::disk('rightHeadshot')->put($RHSfilenameToStore, File::get($request->file('rightHeadshot')));
        //     } else {
        //         $RHSfilenameToStore = 'RHS_noimage.jpg';
        //     }
        // }
        return $user;
    }
}
