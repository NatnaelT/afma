<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use App\ContactRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $authorID = auth()->user()->id;
        $user = User::find($authorID);
        $userList = User::latest()->paginate(10);
        $today = Carbon::today()->toDateString();
        $nowTime = Carbon::now()->toTimeString();
        $upcomingEvents = Event::where('status', false)->where('endDate', '>=', $today)->where('endTime', '>=', $nowTime)->latest()->paginate(3);
        $archivedEvents = Event::where('status', true)->where('endDate', '<', $today)->where('endTime', '>', $nowTime)->latest()->paginate(3);
        $unarchivedEvents = Event::where('status', false)->where('endDate', '<', $today)->where('endTime', '>', $nowTime)->latest()->paginate(3);
        $requests = ContactRequest::where('requestedUserID', $authorID)->where('status', 'pending')->latest()->paginate(3);
        $acceptedContacts = ContactRequest::where('requestingUserID', $authorID)->where('status', 'accepted')->latest()->paginate(3);
        $pendingRegisters = User::where('pending_approval', 'pending')->latest()->paginate(3);
        $accepted = Array();
        foreach($acceptedContacts as $request){
            $accepted = User::where('id', $request->requestedUserID)->latest()->paginate(3);
        }
        // $startTime = $upcomingEvents->startTime;
        // $startTime = date('h:i:sa');
        return view('dashboard')->with('articles', $user->articles)
                                ->with('upcomingEvents', $upcomingEvents)
                                ->with('unarchivedEvents', $unarchivedEvents)
                                ->with('archivedEvents', $archivedEvents)
                                ->with('requests', $requests)
                                ->with('accepted', $accepted)
                                ->with('pendingRegisters', $pendingRegisters);
        // return view('users.userlist')->with('userList', $userList);
    }

    // protected function findAccepted (ContactRequest $contacts){
    //     foreach($contacts as $contact){
    //         $accepted = User::where('id', $contact->requestedUserID)->latest()->paginate(3);
    //     }
    //     return $accepted;
    // }

    public function createRequest($id){
        $requestingID = auth()->user()->id;
        $requestingName = User::find($requestingID);
        $requestedName = User::find($id);

        $req = new ContactRequest;

        $req->requestingUserID = $requestingID;
        $req->requestingUserName = $requestingName->fName.' '.$requestingName->lName;
        $req->requestedUserID = $id;
        $req->requestedUserName = $requestedName->fName.' '.$requestedName->lName;
        $req->status = 'pending';
        $req->save();
        return redirect('/users')->with('success', 'Request has been placed successfully.');
    }

    public function acceptRequest($id){
        $requestedID = auth()->user()->id;
        $contactReq = ContactRequest::where('requestingUserID', $id)
                                    ->where('requestedUserID', $requestedID)
                                    ->where('status', 'pending')->first();

        $contactReq->status = 'accepted';
        $contactReq->save();
        return redirect('/dashboard')->with('success', 'Requested handled successfully.');
    }

    public function rejectRequest($id){
        $requestedID = auth()->user()->id;
        $contactReq = ContactRequest::where('requestingUserID', $id)
                                    ->where('requestedUserID', $requestedID)
                                    ->where('status', 'pending')->first();

        $contactReq->status = 'rejected';
        $contactReq->save();
        return redirect('/dashboard')->with('success', 'Requested handled successfully.');
    }
}
