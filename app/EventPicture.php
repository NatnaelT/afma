<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPicture extends Model
{
    //

    public function event(){
        return $this->belongsTo('App\Event', 'eventID');
    }
}
