<?php

namespace App;

use App\Filters\UserFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fName',
        'lName',
        'bDate',
        'sex',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function articles(){
        return $this->hasMany('App\Article', 'authorID');
    }

    public function events(){
        return $this->hasMany('App\Event', 'userID');
    }

    public function userCredentials(){
        return $this->hasMany('App\UserCredential', 'userID');
    }

    public function userReferences(){
        return $this->hasMany('App\UserReference', 'userID');
    }

    public function scopeSearch($query, $q){
        if($q == null)
        {
            return $query;
        }
        return $query->where('fName', 'LIKE', "%{$q}%")
                     ->orWhere('lName', 'LIKE', "%{$q}%")
                     ->orWhere('email', 'LIKE', "%{$q}%");
    }
}
