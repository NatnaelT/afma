<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserReference extends Model
{
    use HasFactory;
    //
    protected $fillable = [
        'userID', 'jobDescription', 'referenceUserID', 'referencePersonnel'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'userID');
    }
}
