<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ExpireEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for expired or past events by the end of everyday and change their staus to expired.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::now()->toDateString();
        $events = Event::where('endDate', '<', $today)->get();
        foreach($events as $event){
            $event->status = true;
        }
    }
}
