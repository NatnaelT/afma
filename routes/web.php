<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::post('/locale', 'PagesController@changelocale')->name('locale');

//Routes to the index function to landing page.
Route::get('/', 'PagesController@index');

//Creates all the routes to the 'CRUD' functions in the events' controller.
Route::resource('/events', 'EventsController');

//Routes to the members function to members page.
Route::get('/members', 'PagesController@members');

//Routes to the user account function to the user's page. [Needs to a handle for userID]
Route::get('/myaccount', 'PagesController@myaccount');

//Creates all the routes to the 'CRUD' functions in the articles' controller.
Route::resource('articles', 'ArticlesController');

//Creates all the routes to the 'CRUD' functions in the users' controller.
Route::resource('users', 'UsersController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::post('users/{id}', 'DashboardController@createRequest')->name('dashboard.createRequest');

Route::post('dashboard/{id?}/accept', 'DashboardController@acceptRequest')->name('dashboard.acceptRequest');

Route::post('dashboard/{id?}/reject', 'DashboardController@rejectRequest')->name('dashboard.rejectRequest');

Route::get('events/archive/{id}/edit', 'EventsController@archiveEventShow')->name('events.archiveEventShow');

Route::post('events/archive/{id}', 'EventsController@archiveEvent')->name('events.archiveEvent');
